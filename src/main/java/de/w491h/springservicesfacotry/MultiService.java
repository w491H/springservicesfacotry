package de.w491h.springservicesfacotry;

import de.w491h.springservicesfacotry.services.ServiceInterface;
import de.w491h.springservicesfacotry.services.ServiceName;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
@AllArgsConstructor
public class MultiService {
    private final Map<String, ServiceInterface> serviceInterfaceMap;

    public String getByEnum(ServiceName serviceName){
        return serviceInterfaceMap.get(serviceName.serviceName).getInfoAboutService();
    }
}
