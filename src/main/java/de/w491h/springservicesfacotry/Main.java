package de.w491h.springservicesfacotry;

import de.w491h.springservicesfacotry.services.ServiceName;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.command.annotation.Command;
import org.springframework.stereotype.Component;


@Component
@AllArgsConstructor
@Slf4j
@Command(command = "run")
public class Main  {

    private final MultiService multiService;

    @Command(command = "run")
    public void run() {
        log.info("Executing factory with Enum \"A_SERVICE\"");
        log.info(multiService.getByEnum(ServiceName.A_SERVICE));
        log.info("Executing factory with Enum \"B_SERVICE\"");
        log.info(multiService.getByEnum(ServiceName.B_SERVICE));

    }
    @Command(command = "specific")
    public String specific(int i) {
        return multiService.getByEnum(ServiceName.valueOfIndex(i));
    }
    @Command(command = "s")
    public String specificSecond(int i){
        return specific(i);
    }
    @Command(command = "a")
    public String runA() {
        return multiService.getByEnum(ServiceName.A_SERVICE);

    }
    @Command(command = "b")
    public String runB() {
        return multiService.getByEnum(ServiceName.B_SERVICE);

    }
}
