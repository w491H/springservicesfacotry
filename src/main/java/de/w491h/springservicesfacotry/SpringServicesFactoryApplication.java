package de.w491h.springservicesfacotry;


import lombok.AllArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.shell.command.annotation.EnableCommand;

@SpringBootApplication
@AllArgsConstructor
@EnableCommand(Main.class)
public class SpringServicesFactoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringServicesFactoryApplication.class, args);
    }



}
