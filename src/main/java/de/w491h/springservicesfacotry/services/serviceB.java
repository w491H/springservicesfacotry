package de.w491h.springservicesfacotry.services;

import org.springframework.stereotype.Service;

@Service(ServiceName.serviceBName)
public class serviceB implements ServiceInterface {
    @Override
    public String getInfoAboutService() {
        return ServiceName.B_SERVICE.serviceName;
    }
}
