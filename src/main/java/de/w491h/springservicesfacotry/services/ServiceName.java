package de.w491h.springservicesfacotry.services;

import java.util.HashMap;
import java.util.Map;

public enum ServiceName {
    A_SERVICE(ServiceName.serviceAName, 1),
    B_SERVICE(ServiceName.serviceBName, 2);


    private static final Map<Integer, ServiceName> BY_INDEX = new HashMap<>();

    static {
        for (ServiceName e : values()) {
            BY_INDEX.put(e.index, e);
        }
    }

    public final String serviceName;
    public final int index;

    public static final String serviceAName = "ServiceA";
    public static final String serviceBName = "ServiceB";




    ServiceName(String serviceName, int index){
        this.serviceName = serviceName;
        this.index = index;
    }
    public static ServiceName valueOfIndex(int index) {
        return BY_INDEX.get(index);
    }

}
