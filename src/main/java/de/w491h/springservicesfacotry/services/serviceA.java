package de.w491h.springservicesfacotry.services;

import org.springframework.stereotype.Service;

@Service(ServiceName.serviceAName)
public class serviceA implements ServiceInterface {
    @Override
    public String getInfoAboutService() {
        return ServiceName.A_SERVICE.serviceName;
    }
}
